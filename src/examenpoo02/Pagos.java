/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenpoo02;


/**
 *
 * @author hp
 */
public class Pagos {
    private int numDocente;
    private String nombre;
    private String domicilio;
    private int nivel;
    private int pagoBaseHoraImpartida;
    private int horasImpartidas;
     
    public Pagos(){
       this.numDocente = 0;
       this.nombre = "";
       this.domicilio = "";
       this.nivel = 0;
       this.pagoBaseHoraImpartida = 0;
       this.horasImpartidas = 0;
    }

    public Pagos(int numDocente, String nombre, String domicilio, int nivel, int pagoBaseHoraImpartida, int HorasImpartidas) {
        this.numDocente = numDocente;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.pagoBaseHoraImpartida = pagoBaseHoraImpartida;
        this.horasImpartidas = horasImpartidas;
    }
    public Pagos(Pagos x){
        this.numDocente = x.numDocente;
        this.nombre = x.nombre;
        this.domicilio = x.domicilio;
        this.nivel = x.nivel;
        this.pagoBaseHoraImpartida = x.pagoBaseHoraImpartida;
        this.horasImpartidas = x.horasImpartidas;
    }

    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getPagoBaseHoraImpartida() {
        return pagoBaseHoraImpartida;
    }

    public void setPagoBaseHoraImpartida(int pagoBaseHoraImpartida) {
        this.pagoBaseHoraImpartida = pagoBaseHoraImpartida;
    }

    public int getHorasImpartidas() {
        return horasImpartidas;
    }

    public void setHorasImpartidas(int horasImpartidas) {
        this.horasImpartidas = horasImpartidas;
    }
    
    public float calcularPagoHorasImpartidas(){
        float pago=0.0f;
        if (this.nivel == 1){
            pago = (this.horasImpartidas * (this.pagoBaseHoraImpartida)* 1.30f);
        }
        else if(this.nivel == 2){
            pago = (this.horasImpartidas * (this.pagoBaseHoraImpartida)* 1.50f);
        }
        else if(this.nivel == 3){
            pago = (this.horasImpartidas * (this.pagoBaseHoraImpartida)* 2.0f);
        }
        else{
            System.out.println("opcion invalida" );
        }
        return pago;
    }
    public float calcularImpuesto(){
        float impuesto = 0.0f;
        impuesto = this.calcularPagoHorasImpartidas()*0.16f;
        return impuesto;
    }
    public float calcularBono(int hijos){
        float bono = 0.0f;
        //int hijos =0;
        if (hijos == 0){
            bono = 0.0f;
        }
        else if (hijos == 1){
            bono = this.calcularPagoHorasImpartidas()*.05f;
        }
        else if (hijos == 2){
            bono = this.calcularPagoHorasImpartidas()*.05f;
        }
        else if (hijos <= 5){
            bono = this.calcularPagoHorasImpartidas()*.10f;
        }
        else if (hijos > 5){
            bono = this.calcularPagoHorasImpartidas()*.20f;
        }
        
        return bono;
    }
    public float calcularTotalPagar(int hijos){
        float totalPagar = 0.0f;
        //float bono= this.calcularBono(horas);
        totalPagar = (this.calcularPagoHorasImpartidas() - this.calcularImpuesto()+ this.calcularBono(hijos));
        
        return totalPagar;
    }
    
}
